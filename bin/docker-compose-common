#!/usr/bin/env bash

print_message() {
  echo -e "\033[1m \e[34m>> \e[39mdcc: $1\e[0m"
}

run_docker_compose() {
  command_prefix=""

  if hash winpty > /dev/null 2>&1; then
    print_message "detected 'winpty', using it..."

    command_prefix="winpty"
  fi

  echo # new line for formatting

  ${command_prefix} docker compose --project-directory "$(pwd)" "${@}"
}

export COMPOSE_IGNORE_ORPHANS=1

dcc_dirname="$(dirname "${0}")"
dcc_dirname="${dcc_dirname%/*}" # remove anything after the last /
export DCC_DIRNAME="${dcc_dirname}"

local_docker_compose_file="./docker-compose.yml"
service="$2" # get service
service_group="${service%%_*}" # remove anything after the first _
compose_path="${dcc_dirname}/docker-compose.${service_group}.yml"

if [ -f "${compose_path}" ]; then
  print_message "service '${service}' detected; using docker-compose.${service_group}.yml ..."

  if [ "${service_group}" = "composer" ] && [ -n "${DCC_COMPOSER_SSH_KEY_PATH}" ]; then
    print_message "service group 'composer' and environment variable DCC_COMPOSER_SSH_KEY_PATH detected; using 'composer_authenticated' service group ..."

    compose_path="${dcc_dirname}/docker-compose.composer_authenticated.yml"
  fi

  docker_compose_parameters="--file ${compose_path}"
  env_file="${dcc_dirname}/docker-compose-common.override.env"

  if [ -f "${local_docker_compose_file}" ]; then
    docker_compose_parameters="${docker_compose_parameters} --file ${local_docker_compose_file}"
  fi

  docker_compose_parameters="${docker_compose_parameters} --env-file ${dcc_dirname}/docker-compose-common.env"

  if [ -f "${env_file}" ]; then
    docker_compose_parameters="${docker_compose_parameters} --env-file ${env_file}"
  fi

  run_docker_compose ${docker_compose_parameters} "${@}"
else
  print_message "falling back to native docker-compose ..."

  run_docker_compose "$@"
fi
