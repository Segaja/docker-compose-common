# docker-compose-common

It contains additional docker-compose files with basic services and tools which are useful in different projects.

The idea is to have one place to maintain these basic services, instead of copy, paste & update these services in every project.


## ❗ DEPRECATED ❗

**This tool is deprecated since [docker compose](https://docs.docker.com/compose/) version 2.20.3 introduced the [top level `include`](https://docs.docker.com/compose/multiple-compose-files/include/) element
which lets you include `docker-compose.yaml` files from different projects which is essentially what docker-compose-common is doing.**


## dependencies

This tool uses [docker compose](https://docs.docker.com/compose/) version >2.18.0.


## general usage

- call `./bin/docker-compose-common` and pass it your normal `docker-compose` parameters
  - **currently it is not supported to pass any special options to the composer command before the service**. The service you want to start has to be on the third position. e..g.: `./bin/docker-compose-common run composer`
- for convenient usage we recommend to create an alias for your `docker-compose` command to use the provided script.
  - example alias: `alias docker-compose-common="/path/to/docker-compose-common/bin/docker-compose-common"`
- do the commands in the root directory of your project you are working in (as usual) and not from the root directory of the `docker-compose-common` repository
- for Windows users: This script checks if [`winpty`](https://github.com/rprichard/winpty) exists on your system and if so automatically uses it


## services

### composer
This service does a php [composer](https://getcomposer.org/) run for your project. The [upstream composer docker image](https://hub.docker.com/_/composer) is used.

To define the correct php you want to use for your composer run, you have add the following lines into your `composer.json` file.
```json
{
  "config": {
    "platform": {
      "php":"7.0.0"
    }
  }
}
```

- composer install (default): `docker-compose-common run composer`
- composer update: `docker-compose-common run composer update`
- any parameter after `docker-compose-common run composer` will be passed directly to composer itself
- there are two targets for composer: `composer` which uses always the latest composer image and `composer_2.2` which runs version 2.2. This version is needed if you run PHP 7.0 in production. See https://github.com/composer/composer/issues/11245 for details.

If you need to use ssh authentication to load package from a private git(lab) instance, then you need to provide the relative path to your ssh key in the environment variable `DCC_COMPOSER_SSH_KEY_PATH`.

### elk
This service group spawns a local [Elasticsearch](https://www.elastic.co/guide/en/elasticsearch/reference/8.5/elasticsearch-intro.html), [Logstash](https://www.elastic.co/guide/en/logstash/current/introduction.html) and [Kibana](https://www.elastic.co/guide/en/kibana/current/introduction.html) stack for handling logs of local applications more nicely.

- To start the elk stack, simply do the following command, wait a minute and then open e.g. http://localhost:5601 in your browser.
```
docker-compose-common up elk
```

- the Elasticsearch stores its data persistently in the `./elk/data` folder
- by default Logstash has the UDP port `9999` open and excepts messages in JSON format to process into the elasticsearch
  - e.g for PHP see the [Monolog Creator](https://github.com/Bigpoint/monolog-creator/) library to send logs via UDP
  - feel free to add more logstash configuration files at the `./elk/logstash_pipeline/` folder
- you can find exported kibana objects at `./elk/kibana_objects/` to quickly import a simple dashboard

### phpcs
This service runs the [php code sniffer](https://github.com/squizlabs/PHP_CodeSniffer) in your project.

You need to add the [squizlabs/php_codesniffer](https://packagist.org/packages/squizlabs/php_codesniffer) dependency to your projects `composer.json`.
Since the binary `./vendor/bin/phpcs` is only present after a composer run, **you must run the composer before** (see above).

- phpcs for PHP version 7.0: `docker-compose-common run phpcs_php-7.0`
- phpcs for PHP version 7.3: `docker-compose-common run phpcs_php-7.3`
- phpcs for PHP version 8.0: `docker-compose-common run phpcs_php-8.0`
- phpcs for PHP version 8.1: `docker-compose-common run phpcs_php-8.1`

To add a project specific phpcs config. Create a `.phpcs.xml` In your projects root folder.

#### example .phpcs.xml
```xml
<?xml version="1.0"?>
<ruleset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" name="PHP_CodeSniffer" xsi:noNamespaceSchemaLocation="phpcs.xsd">
    <arg value="p"/> <!-- show progress -->
    <arg name="extensions" value="php"/>

    <exclude-pattern>./vendor/</exclude-pattern>

    <rule ref="PSR12"></rule>
</ruleset>
```

### phpcbf
This service runs the php code beautifier of the [php code sniffer](https://github.com/squizlabs/PHP_CodeSniffer) in your project.

You need to add the [squizlabs/php_codesniffer](https://packagist.org/packages/squizlabs/php_codesniffer) dependency to your projects `composer.json`.
Since the binary `./vendor/bin/phpcbf` is only present after a composer run, **you must run the composer before** (see above).

- phpcs for PHP version 7.0: `docker-compose-common run phpcbf_php-7.0`
- phpcs for PHP version 7.3: `docker-compose-common run phpcbf_php-7.3`
- phpcs for PHP version 8.0: `docker-compose-common run phpcbf_php-8.0`
- phpcs for PHP version 8.1: `docker-compose-common run phpcbf_php-8.1`

### phpunit
This service runs [phpunit](https://phpunit.de/) for the php unit tests in your project.

You need to add the [phpunit/phpunit](https://packagist.org/packages/phpunit/phpunit) dependency to your projects `composer.json`.
Since the binary `./vendor/bin/phpunit` is only present after a composer run, **you must run the composer before** (see above).

- phpunit for PHP version 7.0: `docker-compose-common run phpunit_php-7.0`
- phpunit for PHP version 7.3: `docker-compose-common run phpunit_php-7.3`
- phpunit for PHP version 8.0: `docker-compose-common run phpunit_php-8.0`
- phpunit for PHP version 8.1: `docker-compose-common run phpunit_php-8.1`

You can find [here](https://phpunit.de/supported-versions.html) the supported phpunit version of the different PHP versions.
By default, phpunit will use the `phpunit.xml` file in the current working dir. Otherwise it has to be set with the `-c` parameter.


### ssl-proxy
This is a small [haproxy](https://www.haproxy.org/) service, with a default ssl configuration to put it before one of the other websites in your local environment.

- It will use the `./ssl-proxy/haproxy.cfg` file from this repository
- You have to create a new self sign certificate if not done yet for this proxy. Please have a look at the next section **ssl-proxy-generate-certificate**
- By default the `ssl-proxy` service will expect a `website` service to be located in your local `docker-compose.yml` file which is running on port `80`. These values can be overwritten with the environment variables `DCC_SSLPROXY_BACKEND_HOST` and `DCC_SSLPROXY_BACKEND_PORT`. See [below](./README.md#override-environment-variables-eg-used-docker-images-and-ssl-proxy-configuration) for details on how to overwrite these variables.
- Start the ssl-proxy and the linked website: `docker-compose-common up ssl-proxy website`
    - The ssl-proxy is then reachable under `https://localhost/`

#### ssl-proxy_generate-certificate
This service generates a new self signed certificate for the given domain

- Example command to generate a new certificate: `DOMAIN=localhost docker-compose-common run ssl-proxy_generate-certificate`

### yarn
This service does a [yarn](https://yarnpkg.com/) run for your project.

- yarn install: `docker-compose-common run yarn`
- yarn upgrade: `docker-compose-common run yarn up`
- any parameter after `docker-compose-common run yarn` will be passed directly to yarn itself


## Override environment variables (e.g. used docker images and ssl-proxy configuration)
For the services in the groups `phpcbf`, `phpcs` and `phpunit` by default the basic [php](https://hub.docker.com/_/php) upstream docker images are used (see [`./docker-compose-common.env`](./docker-compose-common.env)).

If you want to use a different image for any of these services then you can do that with the following steps:

- copy the [`./docker-compose-common.env`](./docker-compose-common.env) file to `./docker-compose-common.override.env`
- edit the image you want to change to any image that provides a php cli
- you can remove the variables you don't want to override as they will then be taken from the [`./docker-compose-common.env`](./docker-compose-common.env) file

You can also run any of these services with a custom PHP image without the override file by doing something like this:

```shell
DCC_PHPUNIT_7_0_IMAGE=my-custom-php-image:version-x /path/to/docker-compose-common/bin/docker-compose-common run phpcs_php-7.0
```


## How to add new services
Each service has to be in its own `docker-compose.<service_group>.yml` file in this repository and the name of the service has to start with the `<service_group>` name.
Any sub services in the same `<service_group>` have to be named `<service_group>_<subservice>`.

Also the `<service_group>` MAY NOT container `_` in its name as this is considered the delimiter between a `<service_group>` and any `<subservice>`.

To find an example for a service group which has multiple services check `docker-compose.phpcs.yml`.

## Tips & Tricks
### VSCode integration
To add docker-compose-common commands in the VSCode command palette:

- install docker-compose-common(see [general usage](#general-usage))
- set an alias in your environment to the docker-compose-common executable (in the following example `dcc` is used)
- install the [Command Runner](https://marketplace.visualstudio.com/items?itemName=edonet.vscode-command-runner) VSCode plugin
- add the following config into your `settings.json` file
  - add other commands as you see fit
```json
{
    "command-runner.terminal.name": "bash",
    "command-runner.terminal.autoClear": true,
    "command-runner.terminal.autoFocus": true,
    "command-runner.commands": {
        "dcc composer install": "dcc run composer",
        "dcc composer update":  "dcc run composer update",
    }
}
```
- use one of the following ways to open the custom commands
  - `ctrl + shift + p` and select `runCommand`
  - `ctrl + shift + r`

